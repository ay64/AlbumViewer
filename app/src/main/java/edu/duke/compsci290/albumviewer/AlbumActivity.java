package edu.duke.compsci290.albumviewer;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

public class AlbumActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        Intent receivedIntent = this.getIntent();
        String albumName = receivedIntent.getStringExtra("album_name_key");
        String artistName = receivedIntent.getStringExtra("artist_name_key");

        /*get album and artist name*/
        ( (TextView)findViewById(R.id.AlbumNameView)).setText(albumName);
        ( (TextView)findViewById(R.id.ArtistNameView)).setText(artistName);

        String s = albumName.toLowerCase().replaceAll("\\W+", "");
        int drawableId = getResources().getIdentifier(s,"drawable", getPackageName());
        Drawable albumArtwork = getDrawable(drawableId);
        ( (ImageView) findViewById(R.id.AlbumPic)).setImageDrawable(albumArtwork);

        String[] Songs = getResources().getStringArray(getResources().getIdentifier(s,"array", getPackageName()));


        RecyclerView rv = findViewById(R.id.SongsRecyclerView);
        rv.setAdapter(new SongAdapter(this, Songs));
        rv.setLayoutManager(new LinearLayoutManager(this));


    }
}
