package edu.duke.compsci290.albumviewer;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Henry_Yu on 2/1/18.
 */

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.ViewHolder>{

    private Context mContext;
    private String [] mSongs;

    public SongAdapter(final Context context, String[] songs){
        this.mContext = context;
        this.mSongs = songs;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mLinearLayout;
        private TextView mSong;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mLinearLayout = itemView.findViewById(R.id.song_holder_linear_layout);
            this.mSong = itemView.findViewById(R.id.SongNameView);
        }
    }

    @Override
    public int getItemCount(){
        return mSongs.length;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = mInflater.inflate(R.layout.song_holder, parent, false);
        final ViewHolder songHolder = new ViewHolder(row);
        return songHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        holder.mSong.setText(mSongs[position]);
    }



}
